# Doc-Censor

## Overview

This webapp takes in 2 txt documents. One containing a string of censored keywords and phrases separated by spaces or commas. Phrases will be enclosed in single or double quotes. The other containing text that needs the provided keywords and phrases removed. The user will click on 2 buttons to upload said files in order and will click on the final button to download the new document with the keywords masked.

## How to run

cd into the frontend directory and run `npm i` and `npm run build`
cd into server directory and run `npm i` and `node main.js`
open browser and go to [localhost](http://localhost:3000/)

## extra info

Ways I would improve this in the short term would be:

- adding proper error handling when parsing the documents and user notifications for said errors
- add unit tests especially with backend parsing custom functions
- deciding if the entire document should be sent to the server vs just the text (would think encode entire doc and sent to server)
- encoding data on frontend and decoding on backend and vice versa
- having the keywords and document-to-censor be included in the same api call so nothing is saved on the server
- pretty up the frontend (reading up on angular style guide)
- clear the document inputs so you can reclick the buttons
