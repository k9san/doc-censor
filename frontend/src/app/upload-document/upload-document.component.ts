import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { postSecretWordList } from '../../utils/api/fileCensorApiUtils.js';
import { ServiceDocumentService } from '../service-document.service';

@Component({
  selector: 'app-upload-document',
  templateUrl: './upload-document.component.html',
  styleUrls: ['./upload-document.component.scss'],
})
export class UploadDocumentComponent implements OnInit {
  @ViewChild('inputFile') inputFile: ElementRef;

  constructor(private documentService: ServiceDocumentService) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    console.log(this.inputFile.nativeElement.value);
  }

  uploadFile() {
    this.inputFile.nativeElement.click();
  }
  onChange($event) {
    this.onFileSelected($event);
  }
  onFileSelected({ target, dataTransfer }) {
    const file = target.files[0];
    file.text().then((textToCensor) => {
      postSecretWordList(textToCensor).then((res) => {
        this.documentService.replaceName('any');
      });
    });
  }
}
