import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UploadDocumentComponent } from './upload-document/upload-document.component';
import { UploadClassifiedListComponent } from './upload-classified-list/upload-classified-list.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { DownloadCensoredDocComponent } from './download-censored-doc/download-censored-doc.component';

@NgModule({
  declarations: [
    AppComponent,
    UploadDocumentComponent,
    UploadClassifiedListComponent,
    ProgressBarComponent,
    DownloadCensoredDocComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
