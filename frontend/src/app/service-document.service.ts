import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ServiceDocumentService {
  textUpdated: EventEmitter<any> = new EventEmitter();
  nameUpdated: EventEmitter<any> = new EventEmitter();

  name: string = '';
  text: string = '';

  replaceName(name) {
    this.name = name;
    this.nameUpdated.emit(this.name);
  }

  getName() {
    return this.name;
  }

  clearName() {
    this.name = '';
  }

  replaceText(text) {
    this.text = text;
    this.textUpdated.emit(this.text);
  }

  getText() {
    return this.text;
  }

  clearText() {
    this.text = '';
  }
}
