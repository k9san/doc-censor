import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadCensoredDocComponent } from './download-censored-doc.component';

describe('DownloadCensoredDocComponent', () => {
  let component: DownloadCensoredDocComponent;
  let fixture: ComponentFixture<DownloadCensoredDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DownloadCensoredDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadCensoredDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
