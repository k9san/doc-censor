import { Component, OnInit } from '@angular/core';
import { ServiceDocumentService } from '../service-document.service';

@Component({
  selector: 'app-download-censored-doc',
  templateUrl: './download-censored-doc.component.html',
  styleUrls: ['./download-censored-doc.component.scss'],
})
export class DownloadCensoredDocComponent implements OnInit {
  constructor(private documentService: ServiceDocumentService) {}

  show = false;

  ngOnInit(): void {
    this.documentService.textUpdated.subscribe(() => {
      this.show = true;
    });
  }

  download() {
    const filename = this.documentService.getName();
    const text = this.documentService.getText();
    console.log(text);

    var element = document.createElement('a');
    element.setAttribute(
      'href',
      'data:text/plain;charset=utf-8,' + encodeURIComponent(text),
    );
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
}
