import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { postCensorFile } from '../../utils/api/fileCensorApiUtils.js';
import { ServiceDocumentService } from '../service-document.service';

@Component({
  selector: 'app-upload-classified-list',
  templateUrl: './upload-classified-list.component.html',
  styleUrls: ['./upload-classified-list.component.scss'],
})
export class UploadClassifiedListComponent implements OnInit {
  @ViewChild('inputFile') inputFile: ElementRef;

  constructor(private documentService: ServiceDocumentService) {}

  show = false;

  ngOnInit(): void {
    this.documentService.nameUpdated.subscribe(() => {
      this.show = true;
    });
  }

  ngAfterViewInit() {
    console.log(this.inputFile.nativeElement.value);
  }

  uploadFile() {
    this.inputFile.nativeElement.click();
  }
  onChange($event) {
    this.onFileSelected($event);
  }
  onFileSelected({ target }) {
    const file = target.files[0];
    file.text().then((textToCensor) => {
      postCensorFile(textToCensor).then((res) => {
        this.documentService.replaceName(file.name);
        this.documentService.replaceText(res.data);
      });
    });
  }
}
