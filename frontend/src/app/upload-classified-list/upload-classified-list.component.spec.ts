import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadClassifiedListComponent } from './upload-classified-list.component';

describe('UploadClassifiedListComponent', () => {
  let component: UploadClassifiedListComponent;
  let fixture: ComponentFixture<UploadClassifiedListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadClassifiedListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadClassifiedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
