import axios from 'axios';

// core api calls
export const postCensorFile = (payload) => {
  return axios.post(`/censorFile`, {
    payload,
  });
};

export const postSecretWordList = (payload) => {
  return axios.post(`/secretWordList`, {
    payload,
  });
};
