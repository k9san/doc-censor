const express = require('express')
const censorHelper = require('./utils/helpers/censorHelper.js')
var app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.set('view engine', 'ejs');
app.use(express.static('../frontend/dist/frontend'));

let secretWordList = [];

app.post('/censorFile', function (req, res) {
  if (!req.body.payload) {
    res.status(400).send('error: empty body');
  } else {
    try {
      const resBody = censorHelper.censor(req.body.payload, secretWordList);
      res.status(200).send(resBody);
    } catch (error) {
      res.status(500).send('whoops something went wrong');
    }
  }
});

app.post('/secretWordList', function (req, res) {
  if (!req.body.payload) {
    res.status(400).send('error: empty body');
  } else {
    try {
      console.log(censorHelper);
      secretWordList = censorHelper.getSecretWords(req.body.payload);
      console.log(secretWordList);
      res.status(200).send('Success');
    } catch (error) {
      console.log(error);
      res.status(500).send('whoops something went wrong');
    }
  }
});

app.listen(3000);
