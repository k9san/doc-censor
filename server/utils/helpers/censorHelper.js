exports.censor = function (param, secretWordList) {
  let ret = param;
  secretWordList.forEach((phrase) => {
    var rg = new RegExp('\\b' + phrase + '\\b', 'g');
    ret = ret.replace(rg, 'xxxx');
  });
  return ret;
};

exports.getSecretWords = function (param) {
  const secretWordList = [];
  let wordToUpdate = '';
  let isPhrase = false;
  [...param].forEach((char) => {
    if (char === '"' || char === "'") {
      if (isPhrase && wordToUpdate) {
        secretWordList.push(wordToUpdate);
        wordToUpdate = '';
      }
      isPhrase = !isPhrase;
      return;
    } else if (char === ' ' && !isPhrase) {
      if (wordToUpdate) {
        secretWordList.push(wordToUpdate);
        wordToUpdate = '';
      }
      return;
    } else if (char === ',') {
      return;
    }
    wordToUpdate += char;
  });
  if (wordToUpdate) {
    secretWordList.push(wordToUpdate);
  }
  return secretWordList;
};
